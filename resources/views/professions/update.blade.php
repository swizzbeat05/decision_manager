@extends('layouts.main')

@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">

        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('professions.update', [$profession->id]) }}"
                enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="codeprofession">Libelé de la profession</label>
                            <input class="form-control {{ $errors->has('codeprofession') ? 'is-invalid' : '' }}"
                                type="text" name="codeprofession" id="codeprofession"
                                value="{{ $profession->codeprofession }}" required>

                            @if ($errors->has('codeprofession'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('codeprofession') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="libprofession">Libelé de la profession</label>
                            <input class="form-control {{ $errors->has('libprofession') ? 'is-invalid' : '' }}"
                                type="text" name="libprofession" id="libprofession"
                                value="{{ $profession->libprofession }}" required>

                            @if ($errors->has('libprofession'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('libprofession') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="categorie_id">Libelé de la catégorie</label>
                            <select class="form-control {{ $errors->has('categorie_id') ? 'is-invalid' : '' }}"
                                name="categorie_id" id="categorie_id">

                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"
                                        @if (old('categorie_id', $category->id) === $profession->categorie_id) {{ 'Selected' }} @endif>
                                        {{ $category->libcat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-danger" type="submit">
                            Validez
                        </button>
                    </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endsection
