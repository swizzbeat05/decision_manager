@extends('layouts.main')

@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">

        </div>


        <div class="card-body">
            <form method="POST" action="{{ route('professions.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="codeprofession">Code de la profession</label>
                            <input class="form-control {{ $errors->has('codeprofession') ? 'is-invalid' : '' }}"
                                type="text" name="codeprofession" id="codeprofession"
                                value="{{ old('codeprofession', '') }}" required>

                            @if ($errors->has('codeprofession'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('codeprofession') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="libprofession">Libellé de la profession</label>
                            <input class="form-control {{ $errors->has('libprofession') ? 'is-invalid' : '' }}"
                                type="text" name="libprofession" id="libprofession"
                                value="{{ old('libprofession', '') }}" required>

                            @if ($errors->has('libprofession'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('libprofession') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="categorie_id">Libellé de la catégorie</label>
                            <select class="form-control {{ $errors->has('categorie_id') ? 'is-invalid' : '' }}"
                                name="categorie_id" id="categorie_id" required>

                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->libcat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger" type="submit">
                            Validez
                        </button>
                    </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endsection
