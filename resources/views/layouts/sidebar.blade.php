<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->


    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">

            </div>
            <div class="info">
                <a href="#" class="d-block"></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview menu-open">
                    <a href="{{ route('home') }}" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Tableau de Board
                        </p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-arrows-alt"></i>
                        <p>
                            Produits de crédit
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('produitcredits.index') }}">Index</a>
                    </ul>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('produitcredits.create') }}">Ajouter un produit</a>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-arrows-alt"></i>
                        <p>
                            Catégories
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('categories.index') }}">Index</a>
                    </ul>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('categories.create') }}">Ajouter une catégorie</a>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-arrows-alt"></i>
                        <p>
                            Profession
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('professions.index') }}">Index</a>
                    </ul>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('professions.create') }}">Ajouter une profession</a>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-arrows-alt"></i>
                        <p>
                            Type de demandeur
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('typedemandeurs.index') }}">Index</a>
                    </ul>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('typedemandeurs.create') }}">Ajouter un type</a>
                    </ul>
                </li>


                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-arrows-alt"></i>
                        <p>
                            Sexe
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('sexes.index') }}">Index</a>
                    </ul>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('sexes.create') }}">Ajouter un sexe</a>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-arrows-alt"></i>
                        <p>
                            Situation matrimoniale
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('sitmatrimoniales.index') }}">Index</a>
                    </ul>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('sitmatrimoniales.create') }}">Ajouter une situation</a>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-arrows-alt"></i>
                        <p>
                            Niveaux
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('niveaux.index') }}">Index</a>
                    </ul>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('niveaux.create') }}">Ajouter un niveau</a>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-arrows-alt"></i>
                        <p>
                            Décision
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('decisions.index') }}">Index</a>
                    </ul>
                    <ul class="nav nav-treeview">
                        <a href="{{ route('decisions.create') }}">Ajouter une décision</a>
                    </ul>
                     <ul class="nav nav-treeview">
                        <a href="{{ route('decisions.afficher_taux') }}">Taux de remboursement</a>
                    </ul>

                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-users"></i>
                        <p>
                            Utilisateurs
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Liste des Utilisateurs</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ajouter Utilisateur</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Modifier mot de passe</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item has-treeview">


                <li class="nav-item">

                    <a class="nav-link" href=""
                        onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

                        <i class="fas fa-sign-out-alt"></i>
                        <p>Déconnexion</p>
                    </a>

                    <form id="logout-form" action="" method="POST" style="display: none;">
                        @csrf
                    </form>

                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
