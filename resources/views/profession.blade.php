@extends("layouts.master")
@section("contenu")

<div class="my-3 p-3 bg-body rounded shadow-sm">
    <h3 class="border-bottom pb-2 mb-4">Liste des professions</h3>
    <div class="mt-4">
      <div class="d-flex justify-content-end mb-4">
        <a href="#" class="btn btn-primary">nouvelle profession</a>
      </div>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Code</th>
            <th scope="col">Désignation</th>
            <th scope="col">Catégorie</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        @foreach ( $professions as $profession )
            
       
          <tr>
            <th scope="row"> {{$profession->id}}</th>
            <td> {{$profession->codeprofession}}</td>
            <td> {{$profession->libprofession}}</td>
            <td> {{$profession->codecat}}</td>
            <td>
              <a href="#" class="btn btn-info">Editer</a>
              <a href="#" class="btn btn-danger">Supprimer</a>

            </td>
          </tr>
          @endforeach  
        </tbody>
      </table>


    </div>

  </div>

 

@endsection