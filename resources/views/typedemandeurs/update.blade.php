@extends('layouts.main')

@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">

        </div>


        <div class="card-body">

            {!! Form::model($typedemandeur, ['method' => 'PATCH', 'route' => ['typedemandeurs.update', $typedemandeur->id]]) !!}

            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="required" for="codetype">Code du type</label>
                        <input class="form-control {{ $errors->has('codetype') ? 'is-invalid' : '' }}" type="text"
                            name="codetype" id="codetype" value="{{ $typedemandeur->codetype }}" required>

                        @if ($errors->has('codetype'))
                            <div class="invalid-feedback">
                                {{ $errors->first('codetype') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="required" for="libtype">Libellé du type</label>
                        <input class="form-control {{ $errors->has('libpc') ? 'is-invalid' : '' }}" type="text"
                            name="libtype" id="libtype" value="{{ $typedemandeur->libtype }}" required>

                        @if ($errors->has('libtype'))
                            <div class="invalid-feedback">
                                {{ $errors->first('libtype') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    Validez
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endsection
