@extends('layouts.main')

@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">

        </div>


        <div class="card-body">

            {!! Form::model($produitcredit, ['method' => 'PATCH', 'route' => ['produitcredits.update', $produitcredit->id]]) !!}

            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="required" for="codepc">Code du produit</label>
                        <input class="form-control {{ $errors->has('codepc') ? 'is-invalid' : '' }}" type="text"
                            name="codepc" id="codepc" value="{{ $produitcredit->codepc }}" required>

                        @if ($errors->has('codepc'))
                            <div class="invalid-feedback">
                                {{ $errors->first('codepc') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="required" for="libpc">Libellé du produit</label>
                        <input class="form-control {{ $errors->has('libpc') ? 'is-invalid' : '' }}" type="text"
                            name="libpc" id="libpc" value="{{ $produitcredit->libpc }}" required>

                        @if ($errors->has('libpc'))
                            <div class="invalid-feedback">
                                {{ $errors->first('libpc') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    Validez
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endsection
