@extends('layouts.main')

@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">

        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('decisions.update', [$decision->id]) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="refdecision">Référence décision</label>
                            <input class="form-control {{ $errors->has('refdecision') ? 'is-invalid' : '' }}"
                                type="text" name="refdecision" id="refdecision" value="{{ $decision->refdecision }}"
                                required>

                            @if ($errors->has('refdecision'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('refdecision') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="categorie_id">Libellé de la catégorie</label>
                            <select class="form-control {{ $errors->has('categorie_id') ? 'is-invalid' : '' }}"
                                name="categorie_id" id="categorie_id">

                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"
                                        @if (old('categorie_id', $category->id) === $decision->categorie_id) {{ 'Selected' }} @endif>
                                        {{ $category->libcat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="typedemandeur_id">Type demandeur</label>
                            <select class="form-control {{ $errors->has('typedemandeur_id') ? 'is-invalid' : '' }}"
                                name="typedemandeur_id" id="typedemandeur_id">

                                @foreach ($typedemandeurs as $typedemandeur)
                                    <option value="{{ $typedemandeur->id }}"
                                        @if (old('typedemandeur_id', $typedemandeur->id) === $decision->typedemandeur_id) {{ 'Selected' }} @endif>
                                        {{ $typedemandeur->libtype }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="niveau_id">Niveau</label>
                            <select class="form-control {{ $errors->has('niveau_id') ? 'is-invalid' : '' }}"
                                name="niveau_id" id="niveau_id">

                                @foreach ($niveaux as $niveau)
                                    <option value="{{ $niveau->id }}"
                                        @if (old('niveau_id', $niveau->id) === $decision->niveau_id) {{ 'Selected' }} @endif>
                                        {{ $niveau->libniv }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="sexe_id">Sexe</label>
                            <select class="form-control {{ $errors->has('sexe_id') ? 'is-invalid' : '' }}" name="sexe_id"
                                id="sexe_id">

                                @foreach ($sexes as $sex)
                                    <option value="{{ $sex->id }}"
                                        @if (old('sexe_id', $sex->id) === $decision->sexe_id) {{ 'Selected' }} @endif>
                                        {{ $sex->libsexe }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="produitcredit_id">Produit</label>
                            <select class="form-control {{ $errors->has('produitcredit_id') ? 'is-invalid' : '' }}"
                                name="produitcredit_id" id="produitcredit_id">

                                @foreach ($produitcredits as $produitcredit)
                                    <option value="{{ $produitcredit->id }}"
                                        @if (old('produitcredit_id', $produitcredit->id) === $decision->produitcredit_id) {{ 'Selected' }} @endif>
                                        {{ $produitcredit->libpc }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="matrimoniale_id">Situation matrimoniale</label>
                            <select class="form-control {{ $errors->has('matrimoniale_id') ? 'is-invalid' : '' }}"
                                name="matrimoniale_id" id="matrimoniale_id">

                                @foreach ($matrimoniales as $sitmatrimoniale)
                                    <option value="{{ $sitmatrimoniale->id }}"
                                        @if (old('matrimoniale_id', $sitmatrimoniale->id) === $decision->matrimoniale_id) {{ 'Selected' }} @endif>
                                        {{ $sitmatrimoniale->libmatri }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="taux">Taux de remboursement</label>
                            <input class="form-control {{ $errors->has('taux') ? 'is-invalid' : '' }}" type="text"
                                name="taux" id="taux" value="{{ $decision->taux }}" required>

                            @if ($errors->has('taux'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('taux') }}
                                </div>
                            @endif
                        </div>
                    </div>



                    <div class="form-group">
                        <button class="btn btn-danger" type="submit">
                            Validez
                        </button>
                    </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endsection
