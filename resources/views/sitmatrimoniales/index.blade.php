@extends('layouts.main')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="col-sm-2 pull-right" style="margin-bottom: 10px">

                    <a href="{{ route('home') }}">
                        <button type="button" class="btn btn-block btn-outline-primary btn-sm">Retour</button>
                    </a>

                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Liste des Etats de vie</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped datatable-Transaction">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Code Etat</th>
                                    <th>Désignation Etat</th>

                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($matrimoniales as $key => $sitmatrimoniale)
                                    <tr>
                                        <td>{{ $sitmatrimoniale->id ?? '' }}</td>

                                        <td>{{ $sitmatrimoniale->codematri ?? '' }}</td>
                                        <td>{{ $sitmatrimoniale->libmatri ?? '' }}</td>


                                        <td class="">
                                            <div class="btn-group btn-group-sm">

                                                {!! Form::open(['route' => ['sitmatrimoniales.destroy', $sitmatrimoniale->id], 'method' => 'delete']) !!}
                                                <div class='btn-group'>

                                                    <a href="{{ route('sitmatrimoniales.edit', $sitmatrimoniale->id) }}"
                                                        class='btn btn-warning'>
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    {!! Form::button('<i class="fa fa-trash"></i>', [
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-sm  btn-square btn-danger',
                                                        'onclick' => "return confirm('Êtes vous sûr de supprimer ?')",
                                                    ]) !!}

                                                </div>
                                                {!! Form::close() !!}

                                            </div>
                                        </td>


                                    </tr>
                                @endforeach



                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>Code du sexe</th>
                                    <th>Désignation du sexe</th>

                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


    </section>
@endsection
@section('js')
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>

    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                dom: 'Bfrtip',
                buttons: [

                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });





        });
    </script>
@endsection
