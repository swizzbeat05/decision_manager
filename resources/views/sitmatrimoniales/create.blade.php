@extends('layouts.main')

@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">

        </div>


        <div class="card-body">
            <form method="POST" action="{{ route('sitmatrimoniales.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="codematri">Code Etat</label>
                            <input class="form-control {{ $errors->has('codematri') ? 'is-invalid' : '' }}" type="text"
                                name="codematri" id="codematri" value="{{ old('codematri', '') }}" required>

                            @if ($errors->has('codematri'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('codematri') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required" for="libmatri">Libellé Etat</label>
                            <input class="form-control {{ $errors->has('libmatri') ? 'is-invalid' : '' }}" type="text"
                                name="libmatri" id="libmatri" value="{{ old('libmatri', '') }}" required>

                            @if ($errors->has('libmatri'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('libmatri') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" type="submit">
                        Validez
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endsection
