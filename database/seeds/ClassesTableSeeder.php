<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("produitcredits")->insert([
            ["codepc"=>"001"],["libpc"=>"Prêts aux salariés/Fonctionnaires"],
            ["codepc"=>"002"],["libpc"=>"Prêts aux commerçants et artisans"],
        ]);
    }
}
