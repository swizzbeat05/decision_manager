<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('codeprofession');
            $table->string('libprofession')->nullable();
            $table->unsignedBigInteger('categorie_id');
            #$table->index(["categorie_id"], 'fk_categories_has_Professions_Categorie1_idx');
            #$table->foreignId('categorie_id')->constrained("categories");
            $table->foreign('categorie_id', 'fk_Professions_Categorie1_idx')
                ->references('id')->on('categories');

            $table->timestamps();
        });
        # Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        #  Schema::table("professions", function (Blueprint $table) {
        #      $table->dropForeign("categorie_id");
        # });
        Schema::dropIfExists('professions');
    }
}
