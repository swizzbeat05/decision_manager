<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decisions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('refdecision');
            $table->string('taux')->nullable();
            $table->unsignedBigInteger('categorie_id');
            $table->foreign('categorie_id')->references('id')->on('categories');

            $table->unsignedBigInteger('typedemandeur_id');
            $table->foreign('typedemandeur_id')->references('id')->on('typedemandeurs');

            $table->unsignedBigInteger('niveau_id');
            $table->foreign('niveau_id')->references('id')->on('niveaux');

            $table->unsignedBigInteger('sexe_id');
            $table->foreign('sexe_id')->references('id')->on('sexes');

            $table->unsignedBigInteger('produitcredit_id');
            $table->foreign('produitcredit_id')->references('id')->on('produitcredits');

            $table->unsignedBigInteger('matrimoniale_id');
            $table->foreign('matrimoniale_id')->references('id')->on('matrimoniales');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decisions');
    }
}
