<?php


use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('decisions/afficher_taux', 'DecisionController@taux')->name('decisions.afficher_taux');

Route::post('decision/tauxremboursement', 'DecisionController@tauxremboursement')->name('decision.tauxremboursement');

Route::resource('categories', 'CategorieController');

Route::resource('professions', 'ProfessionController');

Route::resource('produitcredits', 'ProduitcreditController');

Route::resource('typedemandeurs', 'TypedemandeurController');

Route::resource('sexes', 'SexeController');

Route::resource('sitmatrimoniales', 'SitmatrimonialeController');

Route::resource('niveaux', 'NiveauController');

Route::resource('decisions', 'DecisionController');




