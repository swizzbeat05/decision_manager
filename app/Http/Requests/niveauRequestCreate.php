<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class niveauRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'codeniv' => 'required',
            'libniv'  => 'required'
        ];
    }

    public function messages()

    {
        return [
            'codeniv.required' => 'Veuillez entrer le code niveau',
            'libniv.required' => 'Veuillez entrer le libellé niveau'
        ];
    }
}
