<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class sexeRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'codesexe' => 'required',
            'libsexe'  => 'required'
        ];
    }

    public function messages()

    {
        return [
            'codesexe.required' => 'Veuillez entrer le code sexe',
            'libsexe.required' => 'Veuillez entrer le libellé sexe'
        ];
    }
}
