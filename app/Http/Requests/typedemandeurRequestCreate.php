<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class typedemandeurRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'codetype' => 'required',
            'libtype'  => 'required'
        ];
    }

    public function messages()

    {
        return [
            'codetype.required' => 'Veuillez entrer le code du typedemandeur',
            'libtype.required' => 'Veuillez entrer le libellé du type demandeur'
        ];
    }
}
