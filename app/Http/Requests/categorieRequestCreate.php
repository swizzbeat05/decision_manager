<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class categorieRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'codecat' => 'required',
            'libcat'  => 'required'
        ];
    }

    public function messages()

    {
        return [
            'codecat.required' => 'Veuillez entrer le code catégorie',
            'libcat.required' => 'Veuillez entrer le libellé de la catégorie'
        ];
    }
}
