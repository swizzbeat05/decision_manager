<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class decisionRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refdecision' => 'required',
            'categorie_id' => 'required|integer',
            'typedemandeur_id' => 'required|integer',
            'niveau_id' => 'required|integer',
            'sexe_id' => 'required|integer',
            'produitcredit_id' => 'required|integer',
            'matrimoniale_id' => 'required|integer',
            'taux' => 'required'
        ];
    }

    public function messages()

    {
        return [
            'codeprofession.required' => 'Veuillez entrer le code de la profession',
            'libprofession.required' => 'Veuillez entrer le libellé de la profession',
            'categorie_id.required' => 'Veuillez entrer le libellé de la catégorie'
        ];
    }
}
