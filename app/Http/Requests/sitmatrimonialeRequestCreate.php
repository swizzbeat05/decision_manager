<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class sitmatrimonialeRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'codematri' => 'required',
            'libmatri'  => 'required'
        ];
    }

    public function messages()

    {
        return [
            'codematri.required' => 'Veuillez entrer le code Etat',
            'libmatri.required' => 'Veuillez entrer le libellé Etat'
        ];
    }
}
