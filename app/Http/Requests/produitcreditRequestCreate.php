<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class produitcreditRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'codepc' => 'required',
            'libpc'  => 'required'

        ];
    }

    public function messages()

    {
        return [
            'codepc.required' => 'Veuillez entrer le code du produit',
            'libpc.required' => 'Veuillez entrer le libellé du produit'
        ];
    }
}
