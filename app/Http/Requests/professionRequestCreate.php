<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class professionRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'codeprofession' => 'required',
            'libprofession'  => 'required',
            'categorie_id' => 'required|integer'
        ];
    }

    public function messages()

    {
        return [
            'codeprofession.required' => 'Veuillez entrer le code de la profession',
            'libprofession.required' => 'Veuillez entrer le libellé de la profession',
            'categorie_id.required' => 'Veuillez entrer le libellé de la catégorie'
        ];
    }
}
