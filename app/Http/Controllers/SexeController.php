<?php

namespace App\Http\Controllers;

use App\Models\Sexe;
use Illuminate\Http\Request;

use App\http\Requests\sexeRequestCreate;

class SexeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sexes = Sexe::all();
        return view('sexes/index', compact('sexes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('sexes/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(sexeRequestCreate $request)
    {
        //
        Sexe::create($request->all());
        return back()->with('success', 'Sexe bien ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sexe  $sexe
     * @return \Illuminate\Http\Response
     */
    public function show(Sexe $sexe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sexe  $sexe
     * @return \Illuminate\Http\Response
     */
    public function edit(Sexe $sex)
    {
        //
        return view('sexes.update', compact('sex'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sexe  $sexe
     * @return \Illuminate\Http\Response
     */
    public function update(sexeRequestCreate $request, Sexe $sex)
    {
        //
        $sexes = Sexe::all();
        $sex->update($request->all());
        \Session::flash('success', 'Mise à jour du sexe effectuée avec succès');
        return redirect()->action('SexeController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sexe  $sexe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sexe $sex)
    {
        //
        $sex->delete();
        $sexes = Sexe::all();
        \Session::flash('success', 'Le sexe a été supprimée avec succès');
        return redirect()->action('SexeController@index');
    }
}
