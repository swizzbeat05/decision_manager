<?php

namespace App\Http\Controllers;

use App\Http\Requests\decisionRequestCreate;
use App\Models\Decision;
use App\Models\Categorie;
use App\Models\Matrimoniale;
use App\Models\Niveau;
use App\Models\Produitcredit;
use App\Models\Sexe;
use App\Models\Typedemandeur;
use Illuminate\Http\Request;

class DecisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $decisions = Decision::all();
        #dd($decisions);
        return view('decisions.index', compact('decisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $matrimoniales = Matrimoniale::all();
        $produitcredits = Produitcredit::all();
        $sexes = Sexe::all();
        $niveaux = Niveau::all();
        $typedemandeurs = Typedemandeur::all();
        $categories = Categorie::all();
        return view('decisions.create')->with('categories', $categories)->with('typedemandeurs', $typedemandeurs)->with('niveaux', $niveaux)->with('sexes', $sexes)->with('produitcredits', $produitcredits)->with('matrimoniales', $matrimoniales);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(decisionRequestCreate $request)
    {
        //
        $inputs = $request->all();
        $inputs['created_at'] = date('Y-m-d H:i:s');

        Decision::create($inputs);

        return redirect('/decisions')->with('success', 'Décision ajoutée avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Decision  $decision
     * @return \Illuminate\Http\Response
     */
    public function show(Decision $decision)
    {
        //
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Decision  $decision
     * @return \Illuminate\Http\Response
     */
    public function edit(Decision $decision)
    {
        //
        $matrimoniales = Matrimoniale::all();
        $produitcredits = Produitcredit::all();
        $sexes = Sexe::all();
        $niveaux = Niveau::all();
        $typedemandeurs = Typedemandeur::all();
        $categories = Categorie::all();
        return view('decisions.update', compact('decision', 'categories', 'typedemandeurs', 'niveaux', 'sexes', 'produitcredits', 'matrimoniales'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Decision  $decision
     * @return \Illuminate\Http\Response
     */
    public function update(decisionRequestCreate $request, Decision $decision)
    {
        //
        $inputs = $request->all();
        $inputs['updated_at'] = date('Y-m-d H:i:s');

        $decision->update($inputs);
        return redirect('/decisions')->with('success', 'Décision mis à jour avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Decision  $decision
     * @return \Illuminate\Http\Response
     */
    public function destroy(Decision $decision)
    {
        //
        $decision->delete();
        $decisions = Decision::all();


        return redirect('/decisions')->with('success', 'Décision supprimée avec succès');
    }

    public function taux(Decision $decision)
    {
        //
        $matrimoniales = Matrimoniale::all();
        $produitcredits = Produitcredit::all();
        $sexes = Sexe::all();
        $niveaux = Niveau::all();
        $typedemandeurs = Typedemandeur::all();
        $categories = Categorie::all();
        return view('decisions.afficher_taux', compact('decision', 'categories', 'typedemandeurs', 'niveaux', 'sexes', 'produitcredits', 'matrimoniales'));
      

    } 

    public function tauxremboursement(Decision $decision)

    {
        //
       Decision::where('id', $decision->id)->select('taux')->get();

    }






}
