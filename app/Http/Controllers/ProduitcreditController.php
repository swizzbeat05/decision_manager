<?php

namespace App\Http\Controllers;

use App\Models\Produitcredit;
use Illuminate\Http\Request;

use App\Http\Requests\produitcreditRequestCreate;

class ProduitcreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $produitcredits = Produitcredit::all();
        return view('produitcredits/index', compact('produitcredits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('produitcredits/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(produitcreditRequestCreate $request)
    {
        //
        Produitcredit::create($request->all());
        return back()->with('success', 'Produit bien ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produitcredit  $produitcredit
     * @return \Illuminate\Http\Response
     */
    public function show(Produitcredit $produitcredit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produitcredit  $produitcredit
     * @return \Illuminate\Http\Response
     */
    public function edit(Produitcredit $produitcredit)
    {
        //
        return view('produitcredits.update', compact('produitcredit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produitcredit  $produitcredit
     * @return \Illuminate\Http\Response
     */
    public function update(produitcreditRequestCreate $request, Produitcredit $produitcredit)
    {
        //
        $produitcredits = Produitcredit::all();
        $produitcredit->update($request->all());
        \Session::flash('success', 'Mise à jour de la catégorie effectuée avec succès');
        return redirect()->action('ProduitcreditController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produitcredit  $produitcredit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produitcredit $produitcredit)
    {
        //
        $produitcredit->delete();
        $produitcredits = Produitcredit::all();
        \Session::flash('success', 'La catégorie a été supprimée avec succès');
        return redirect()->action('ProduitcreditController@index');
    }
}
