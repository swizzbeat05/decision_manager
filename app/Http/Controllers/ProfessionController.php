<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Profession;
use Illuminate\Http\Request;

use App\http\Requests\professionRequestCreate;

class ProfessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $professions = Profession::all();
        return view('professions.index', compact('professions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Categorie::all();
        return view('professions.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(professionRequestCreate  $request)
    {
        //
        $inputs = $request->all();
        $inputs['created_at'] = date('Y-m-d H:i:s');

        Profession::create($inputs);

        return redirect('/professions')->with('success', 'Profession ajoutée avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function show(Profession $profession)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function edit(Profession $profession)
    {
        //
        $categories = Categorie::all();
        return view('professions.update', compact('profession', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function update(professionRequestCreate $request, Profession $profession)
    {
        //
        $inputs = $request->all();
        $inputs['updated_at'] = date('Y-m-d H:i:s');

        $profession->update($inputs);
        return redirect('/professions')->with('success', 'Profession mis à jour avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profession $profession)
    {
        //
        $profession->delete();
        $professions = Profession::all();


        return redirect('/professions')->with('success', 'Profession supprimée avec succès');
    }
}
