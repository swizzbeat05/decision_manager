<?php

namespace App\Http\Controllers;

use App\Models\Typedemandeur;
use Illuminate\Http\Request;

use App\http\Requests\typedemandeurRequestCreate;

class TypedemandeurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $typedemandeurs = Typedemandeur::all();
        return view('typedemandeurs/index', compact('typedemandeurs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('typedemandeurs/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(typedemandeurRequestCreate $request)
    {
        //
        Typedemandeur::create($request->all());
        return back()->with('success', 'Type demandeur bien ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Typedemandeur  $typedemandeur
     * @return \Illuminate\Http\Response
     */
    public function show(Typedemandeur $typedemandeur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Typedemandeur  $typedemandeur
     * @return \Illuminate\Http\Response
     */
    public function edit(Typedemandeur $typedemandeur)
    {
        //
        return view('typedemandeurs.update', compact('typedemandeur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Typedemandeur  $typedemandeur
     * @return \Illuminate\Http\Response
     */
    public function update(typedemandeurRequestCreate $request, Typedemandeur $typedemandeur)
    {
        //
        $typedemandeurs = Typedemandeur::all();
        $typedemandeur->update($request->all());
        \Session::flash('success', 'Mise à jour du type demandeur effectuée avec succès');
        return redirect()->action('TypedemandeurController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Typedemandeur  $typedemandeur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Typedemandeur $typedemandeur)
    {
        //
        $typedemandeur->delete();
        $typedemandeurs = Typedemandeur::all();
        \Session::flash('success', 'La catégorie a été supprimée avec succès');
        return redirect()->action('TypedemandeurController@index');
    }
}
