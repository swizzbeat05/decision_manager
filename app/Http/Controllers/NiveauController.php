<?php

namespace App\Http\Controllers;

use App\Models\Niveau;
use Illuminate\Http\Request;

use App\http\Requests\niveauRequestCreate;

class NiveauController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $niveaux = Niveau::all();

        return view('niveaux/index', compact('niveaux'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('niveaux/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(niveauRequestCreate $request)
    {
        //
        Niveau::create($request->all());
        return back()->with('success', 'Niveau bien ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Niveau  $niveau
     * @return \Illuminate\Http\Response
     */
    public function show(Niveau $niveau)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Niveau  $niveau
     * @return \Illuminate\Http\Response
     */
    public function edit(Niveau $niveau)
    {
        //
        return view('niveaux.update', compact('niveau'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Niveau  $niveau
     * @return \Illuminate\Http\Response
     */
    public function update(niveauRequestCreate $request, Niveau $niveau)
    {
        //
        $niveaux = Niveau::all();
        $niveau->update($request->all());
        \Session::flash('success', 'Mise à jour du niveau effectuée avec succès');
        return redirect()->action('NiveauController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Niveau  $niveau
     * @return \Illuminate\Http\Response
     */
    public function destroy(Niveau $niveau)
    {
        //
        $niveau->delete();
        $niveaux = Niveau::all();
        \Session::flash('success', 'Le niveau a été supprimée avec succès');
        return redirect()->action('NiveauController@index');
    }
}
