<?php

namespace App\Http\Controllers;

use App\Models\Matrimoniale;
use Illuminate\Http\Request;

use App\Http\Requests\sitmatrimonialeRequestCreate;

class SitmatrimonialeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $matrimoniales = Matrimoniale::all();

        return view('sitmatrimoniales/index', compact('matrimoniales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('sitmatrimoniales/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(sitmatrimonialeRequestCreate $request)
    {
        //
        Matrimoniale::create($request->all());
        return back()->with('success', 'Etat de vie bien ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Matrimoniale  $matrimoniale
     * @return \Illuminate\Http\Response
     */
    public function show(Matrimoniale $sitmatrimoniale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Matrimoniale  $matrimoniale
     * @return \Illuminate\Http\Response
     */
    public function edit(Matrimoniale $sitmatrimoniale)
    {
        //
        return view('sitmatrimoniales.update', compact('sitmatrimoniale'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Matrimoniale  $matrimoniale
     * @return \Illuminate\Http\Response
     */
    public function update(sitmatrimonialeRequestCreate $request, Matrimoniale $sitmatrimoniale)
    {
        //
        $matrimoniales = Matrimoniale::all();
        $sitmatrimoniale->update($request->all());
        \Session::flash('success', 'Mise à jour de l\'état de vie effectuée avec succès');
        return redirect()->action('SitmatrimonialeController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Matrimoniale  $matrimoniale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matrimoniale $sitmatrimoniale)
    {
        //
        $sitmatrimoniale->delete();
        $matrimoniales = Matrimoniale::all();
        \Session::flash('success', 'La situation matrimoniale a été supprimée avec succès');
        return redirect()->action('SitmatrimonialeController@index');
    }
}
