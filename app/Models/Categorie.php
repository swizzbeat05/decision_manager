<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    //

    protected $table = 'categories';

    protected $fillable = [
        'codecat',
        'libcat',
        'created_at',
        'updated_at'

    ];
}
