<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Typedemandeur extends Model
{
    //

    protected $table = 'typedemandeurs';

    protected $fillable = [
        'codetype',
        'libtype',
        'created_at',
        'updated_at'

    ];
}
