<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    //

    protected $table = 'professions';

    protected $fillable = [
        'codeprofession',
        'libprofession',
        'categorie_id',
        'created_at',
        'updated_at'
    ];

    public function categorie()
    {
        return $this->belongsTo(Categorie::class);
    }
}
