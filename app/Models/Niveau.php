<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Niveau extends Model
{
    //
    protected $table = 'niveaux';

    protected $fillable = [
        'codeniv',
        'libniv',
        'created_at',
        'updated_at'

    ];
}
