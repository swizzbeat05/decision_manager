<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Decision extends Model
{
    //
    protected $table = 'decisions';

    protected $fillable = [
        'refdecision',
        'categorie_id',
        'typedemandeur_id',
        'niveau_id',
        'sexe_id',
        'produitcredit_id',
        'matrimoniale_id',
        'taux',
        'created_at',
        'updated_at'
    ];

    public function categorie()
    {
        return $this->belongsTo(Categorie::class, 'categorie_id');
    }

    public function Typedemandeur()
    {
        return $this->belongsTo(Typedemandeur::class, 'typedemandeur_id');
    }

    public function niveau()
    {
        return $this->belongsTo(Niveau::class, 'niveau_id');
    }

    public function sexe()
    {
        return $this->belongsTo(Sexe::class, 'sexe_id');
    }

    public function produitcredit()
    {
        return $this->belongsTo(Produitcredit::class, 'produitcredit_id');
    }

    public function situationmatrimoniale()
    {
        return $this->belongsTo(Matrimoniale::class, 'matrimoniale_id');
    }
}
