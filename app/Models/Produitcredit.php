<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produitcredit extends Model
{
    //
    protected $table = 'produitcredits';

    protected $fillable = [
        'codepc',
        'libpc',
        'created_at',
        'updated_at'
    ];
}
