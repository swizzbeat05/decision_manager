<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sexe extends Model
{
    //
    protected $table = 'sexes';

    protected $fillable = [
        'codesexe',
        'libsexe',
        'created_at',
        'updated_at'

    ];
}
