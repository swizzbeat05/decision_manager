<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matrimoniale extends Model
{
    //
    protected $table = 'matrimoniales';

    protected $fillable = [
        'codematri',
        'libmatri',
        'created_at',
        'updated_at'

    ];
}
